#include <iostream>
#include <string>

void greet(std::string salutation, std::string entity) {
    std::cout << salutation << ", " << entity << " World!" << std::endl;
}

int main(void) {
    greet("Hello", "BitBucket");
    return 0;
}

